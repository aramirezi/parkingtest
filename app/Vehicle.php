<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'vehicles';
	public $incrementing = true;
	public $timestamps = true;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
        'id_owner',
        'license_plate',
        'id_brand',
        'id_type_vehicle',
	];
}
