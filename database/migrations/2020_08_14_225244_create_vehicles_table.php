<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_owner')->index();
            $table->foreign('id_owner')->references('id')->on('owners')->onDelete('cascade');
            $table->string('license_plate',6);
            $table->unsignedBigInteger('id_brand')->index();
            $table->foreign('id_brand')->references('id')->on('brands')->onDelete('cascade');
            $table->unsignedBigInteger('id_type_vehicle')->index();
            $table->foreign('id_type_vehicle')->references('id')->on('types_vehicles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
