<?php

use Illuminate\Database\Seeder;

class BrandsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            'description' => 'KIA',
            'status' => 1,
        ]);
        DB::table('brands')->insert([
            'description' => 'RENAULT',
            'status' => 1,
        ]);
        DB::table('brands')->insert([
            'description' => 'CHEVROLET',
            'status' => 1,
        ]);
        DB::table('brands')->insert([
            'description' => 'MAZDA',
            'status' => 1,
        ]);
        DB::table('brands')->insert([
            'description' => 'FORD',
            'status' => 1,
        ]);
        DB::table('brands')->insert([
            'description' => 'NISSAN',
            'status' => 1,
        ]);
        DB::table('brands')->insert([
            'description' => 'SKODA',
            'status' => 1,
        ]);
        DB::table('brands')->insert([
            'description' => 'VOLKSWAGEN',
            'status' => 1,
        ]);
        DB::table('brands')->insert([
            'description' => 'MERCEDES BENZ',
            'status' => 1,
        ]);
        DB::table('brands')->insert([
            'description' => 'TOYOTA',
            'status' => 1,
        ]);
        DB::table('brands')->insert([
            'description' => 'BMW',
            'status' => 1,
        ]);
        DB::table('brands')->insert([
            'description' => 'AUDI',
            'status' => 1,
        ]);
    }
}
