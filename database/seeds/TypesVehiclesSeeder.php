<?php

use Illuminate\Database\Seeder;

class TypesVehiclesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types_vehicles')->insert([
            'description' => 'Campero o Camioneta',
            'status' => 1,
        ]);
        DB::table('types_vehicles')->insert([
            'description' => 'Oficiales, Especiales, Ambulancia, Bomberos, Diplomaticos',
            'status' => 1,
        ]);
        DB::table('types_vehicles')->insert([
            'description' => 'Automóviles Familiares',
            'status' => 1,
        ]);
        DB::table('types_vehicles')->insert([
            'description' => 'Vehículos para 6 o más personas',
            'status' => 1,
        ]);
        DB::table('types_vehicles')->insert([
            'description' => 'Atos de negocio,Taxis y Mircrobusetas urbanos',
            'status' => 1,
        ]);
    }
}
