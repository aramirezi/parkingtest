<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'owners';
	public $incrementing = true;
	public $timestamps = true;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
        'id_type_document',
        'num_document',
        'name',
        'last_name',
        'phone',
        'email',
	];
}
