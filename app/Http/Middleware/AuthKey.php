<?php

namespace App\Http\Middleware;

use Closure;

class AuthKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->header('authorization');

        if($token != 'APP_PARKING_ekOa8Hkkn*u<k6*If'){
            return response()->json(['mensaje'=>'Debes suministar authorization header para acceder a este recurso'],401);
        }

        return $next($request);
    }
}
