<?php

namespace App\Http\Controllers\OwnerVehicle;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Owner;
use App\Vehicle;
use Validator;

class OwnerVehicleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(Request $request)
    {
        // Validación de datos
        $validator = Validator::make($request->all(),[
            'id_type_document' => 'required|numeric|digits_between:1,2:',
            'num_document' => 'required|numeric|digits_between:1,11',
            'name' => 'required|max:200',
            'last_name' => 'required|max:200',
            'phone' => 'numeric|digits_between:11',
            'email' => 'max:191',
            'id_brand' => 'required|numeric',
            'id_type_vehicle' => 'required|numeric',
            'license_plate' => 'required|max:6',
            
        ],[ 
            'id_type_document.required' =>'El id_type_document es obligatorio.', 
            'id_type_document.numeric' =>'El id_type_document debe ser numérico.', 
            'id_type_document.digits_between' =>'El campo id_type_document solo acepta :max caracteres.',     
            'num_document.required' =>'El num_document es obligatorio.', 
            'num_document.numeric' =>'El num_document debe ser numérico.',  
            'num_document.digits_between' =>'El campo num_document solo acepta :max caracteres.',  
            'name.required' =>'El name es obligatorio.', 
            'name.max' =>'El campo name solo acepta :max caracteres.',  
            'last_name.required' =>'El last_name es obligatorio.', 
            'last_name.max' =>'El campo last_name solo acepta :max caracteres.',  
            'phone.digits_between' =>'El campo phone solo acepta :max caracteres.', 
            'email.max' =>'El campo email solo acepta :max caracteres.', 
            'id_brand.required' =>'El id_brand es obligatorio.', 
            'id_brand.numeric' =>'El id_brand debe ser numérico.',   
            'id_type_vehicle.required' =>'El id_type_vehicle es obligatorio.', 
            'id_type_vehicle.numeric' =>'El id_type_vehicle debe ser numérico.', 
            'license_plate.required' =>'El license_plate es obligatorio.', 
            'license_plate.max' =>'El campo license_plate solo acepta :max caracteres.', 
        ]);
        
        if($validator->fails()){         
            return response()->json([
                'code'=> 409,
                'type' => 'error',
                'msg' => $validator->messages()
            ],409); 
        }

        try{

            \DB::beginTransaction();

            // Creamos o consultamos el propietario
            $owner = $this->createOwner($request);
            $owner = json_decode($owner->getContent());
            if( $owner->status != 1){
                return response()->json([
                    'code'=> 409,
                    'type' => 'error',
                    'msg' => 'Error al crear el propietario',
                ],409);
            }

            // Creamos o consultamos el vehículo
            $vehicle = $this->createVehicle($request,$owner->data->id);
            $vehicle = json_decode($vehicle->getContent());
            if( $vehicle->status != 1){
                return response()->json([
                    'code'=> 409,
                    'type' => 'error',
                    'msg' => 'Error al crear el vehículo',
                ],409);
            }

            \DB::commit();

            return response()->json([
                'code'=> 200,
                'type' => 'success',
                'msg' => 'Vehículo creado con éxito',
                'data' => [
                    'owner' => $owner->data,
                    'vehicle' => $vehicle->data,
                ]
            ]);

        } // try
        catch(QueryException $e){

            \DB::rollback();

            return response()->json([
                'code'=> 500,
                'type' => 'error',
                'msg' => $e->getMessage()
            ]);

        }//cath
        
    } // create

    /**
     * Metodo para crear un propietario
     * 
     * @param $request  request parametros petición
     */
    function createOwner($request){

        try{   

            // Consultamos el propietario antes de crearlo
            $find = Owner::where('id_type_document','=',$request->id_type_document)
                        ->where('num_document','=',$request->num_document)
                        ->first();

            if($find){
                return response()->json([
                    'status'=> 1,
                    'type' => 'success',
                    'data' => $find
                ]); 
            }

            // Creamos el propietario
            $owner = Owner::create([
                'id_type_document' => $request->id_type_document,
                'num_document' => $request->num_document,
                'name' => $request->name,
                'last_name' => $request->last_name,
                'phone' => $request->phone,
                'email2' => $request->email
            ]);

            return response()->json([
                'status'=> 1,
                'type' => 'success',
                'data' => $owner
            ]);
        }
        catch(QueryException $e){
            
            return response()->json([
                'status'=> 0,
                'type' => 'error',
                'msg' => $e->getMessage()
            ]);
        }

    } // createOwner


    /**
     * Metodo para crear un vehículo asocuiado a un propietario
     * 
     * @param $request  parametros petición
     * @param $id_owner id del propietario del vehículo
     */
    function createVehicle($request,$id_owner){

        try{   
            // Consultamos el vehículo antes de crearlo
            $find = Vehicle::where('id_owner','=',$id_owner)
                        ->where('license_plate','=',$request->license_plate)
                        ->first();

            if($find){
                return response()->json([
                    'status'=> 1,
                    'type' => 'success',
                    'data' => $find
                ]); 
            }

            // Creamos el vehículo
            $vehicle = Vehicle::create([
                'id_owner' => $id_owner,
                'license_plate' => $request->license_plate,
                'id_brand' => $request->id_brand,
                'id_type_vehicle' => $request->id_type_vehicle
            ]);

            return response()->json([
                'status'=> 1,
                'type' => 'success',
                'data' => $vehicle
            ]);
        }
        catch(QueryException $e){
            return response()->json([
                'status'=> 0,
                'type' => 'error',
                'msg' => $e->getMessage()
            ]);
        }

    } // createVehicle

    function searchVehicle(Request $request){

        // Validación de datos
        $validator = Validator::make($request->all(),[
            'num_document' => 'numeric',
            'license_plate' => 'required|max:6',
            
        ],[     
            'num_document.numeric' =>'El num_document debe ser numérico.',   
            'license_plate.max' =>'El campo license_plate solo acepta :max caracteres.', 
        ]);
        
        if($validator->fails()){         
            return response()->json([
                'code'=> 409,
                'type' => 'error',
                'msg' => $validator->messages()
            ],409); 
        }

                
        $result = Vehicle::join('owners', 'vehicles.id_owner', '=', 'owners.id');
    
        if(isset($request->license_plate)){
            $result = $result->where('vehicles.license_plate','=', $request->license_plate);
        }

        if(isset($request->num_document)){
            $result = $result->where('owners.num_document','=', $request->num_document);
        }

        if(isset($request->name)){
            $result = $result->where('owners.name','=', $request->name);
        }

        $result = $result->select('vehicles.*','owners.*')
                         ->get();

 
        return response()->json([
            'status'=> 1,
            'type' => 'success',
            'data' => $result
        ],200);

    } // searchVehicle
}